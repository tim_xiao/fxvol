# fxVol

An implied volatility is the volatility implied by the market price of an option based on the Black-Scholes option pricing model. A volatility surface is derived from quoted volatilities that provides a way to interpolate an implied volatility at any